import java.io.File;
import java.io.FileFilter;
import java.util.Arrays;
import org.apache.commons.io.filefilter.WildcardFileFilter;

public class ImageFinderCounter extends ImageFinder
{

	public ImageFinderCounter(String rootFolder)
	{
		super(rootFolder);
		// TODO Auto-generated constructor stub
	}

	public String getImagePath(int imageCounter)
	{
		String path = null;

		for (int i = currentYear; i < years.length; i++) {
			if (years[i] != null) {

				for (int j = currentMonth; j < month[i].length; j++) {
					if (month[i][j]) {
						// System.out.println("Patth: " + rootFolder + years[i] + "/"
						// + monthNames[j] + "/" );
						File dir = new File(
								rootFolder + years[i] + "/" + monthNames[j] + "/");
						FileFilter fileFilter = new WildcardFileFilter(
								"*" + convertImageCounter(imageCounter) + "*.jpg");
						File[] files = dir.listFiles(fileFilter);
						path = Arrays.toString(files);
						path = path.replace("[", "");
						path = path.replace("]", "");
						if ((path == "null") || (files.length == 0)) {
							// Nicht gefunden
							// System.out.println("error404");
						} else {
							// Gefunden
							currentMonth = j;
							currentYear = i;
							return path.substring(lengthRootFolder);
						}
					}
				}
				currentMonth = 0;
			}
		}
		return "404";
	}

}
