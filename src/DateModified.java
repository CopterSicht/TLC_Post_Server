import java.io.File;
import java.util.Calendar;
import java.util.Date;

public class DateModified
{
	public Calendar getDate(String filepath)
	{
		Calendar calendar = Calendar.getInstance();

		// Specify the file path and name
		File file = new File(filepath);
		long lastModified = file.lastModified();
		Date date = new Date(lastModified);
		calendar.setTime(date);
		return calendar;

	}
}
