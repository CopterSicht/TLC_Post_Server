import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Calendar;



public class Main
{
	// read from UI later
	static String rootFolderPath; // = "XXX/home/hendrik/FTP/camera01/";

	// read from UI later
	static String projectPath; // = "XXX/home/hendrik/Schreibtisch/";

	// read from UI later
	static String outputPath; // = "XXX/home/hendrik/Schreibtisch/testeritis/";

	// read from UI later
	static int maxImgageCounter; // = 83584; // read from UI later 16819

	static boolean log = false; // Don´t output image counter

	static boolean modi = true; // Create Database // False -> Copy/Link images

	static boolean modi_metadata = true; // true --> exif , false --> timestamp
														// from filesystem

	// java -jar TLC_PostSW.jar -log -m createDB -r /home/hendrik/FTP/camera01/
	// -p /home/hendrik/Schreibtisch/ -o /home/hendrik/Schreibtisch/testeritis/
	// -n 83584
	public static void main(String[] args)
	{

		ImageFinder imgF = null;

		for (int i = 0; i < args.length; i++) {
			// System.out.println(i + ": " +args[i]);

			String value = args[i];

			if (value.equals("-log")) {
				System.out.println("Logging will be used!");
				log = true;
			}

			if (value.equals("-m")) {
				i++;
				if (args[i].equals("createDB")) {
					modi = true;
					System.out.println("[Modi] Index images and create DB");

				} else {
					modi = false;
					System.out.println("[Modi] Copy or Link images");
				}
			}

			if (value.equals("-d")) {
				i++;
				if (args[i].equals("exif")) {
					modi_metadata = true;
					System.out.println("[Metadata] Use Exif for timestamp");
				} else {
					modi_metadata = false;
					System.out.println("[Metadata] Use filesystem for timestamp");
				}
			}

			if (value.equals("-r")) {
				i++;
				rootFolderPath = args[i];
				System.out.println("[rootFolderPath] " + rootFolderPath);
			}

			if (value.equals("-p")) {
				i++;
				projectPath = args[i];
				System.out.println("[projectPath] " + projectPath);
			}

			if (value.equals("-o")) {
				i++;
				outputPath = args[i];
				System.out.println("[outputPath] " + outputPath);
			}

			if (value.equals("-i")) {
				i++;
				if (args[i].equals("counter")) {
					System.out.println("[Identify] Find images with counter");
					imgF = new ImageFinderCounter(rootFolderPath);
				} else {
					System.out.println("[Identify] Find images with date and time");
					imgF = new ImageFinderTimestamp(rootFolderPath);
				}
			}

			if (value.equals("-n")) {
				i++;
				maxImgageCounter = Integer.parseInt(args[i]);
				System.out.println("[maxImgageCounter] " + maxImgageCounter);
			}

			if (value.equals("-help")) {
				System.out.println(
						"-log -m createDB -r /home/hendrik/FTP/camera01/ -p /home/hendrik/Schreibtisch/ -o /home/hendrik/Schreibtisch/testeritis/ -n 83584");
				System.exit(1);
			}

		}

		if (modi) {
			long timeStart;

			DBController db = new DBController(projectPath);
			Exif exif = new Exif();
			DateModified datemod = new DateModified();

			db.connectDatabase();
			db.createTableImages();

			String startyear = imgF.findStartyear();
			int yearCount = Integer.parseInt(startyear);

			imgF.findYears(yearCount, startyear);
			imgF.findMonths(yearCount);

			if (log = true) {
				imgF.printFoundDirectories();
			}

			timeStart = System.currentTimeMillis();
			System.out.println("Suche ...");

			// System.exit(1);

			try {
				PrintWriter missingTxt = new PrintWriter(
						projectPath + "missingImages.txt");
				missingTxt.println("Nicht gefundene Bilder: \n");
				float multiplicator = (float) (100.0 / maxImgageCounter);
				System.out.println("Multiplicator: " + multiplicator);
				for (int i = 0; i <= maxImgageCounter; i++) {
					if (log == true) {
						float percent = multiplicator * i;
						System.out.print(i + "/" + maxImgageCounter + "  --> ");
						System.out.printf("%.2f", percent);
						System.out.println("%");
					}
					String path = imgF.getImagePath(i);
					// System.out.println(i + " Path: " + path);
					if (path != "404") {
						// gefunden
						File f = new File(rootFolderPath + path);
						int size = (int) f.length();
						Calendar calendar;

						if (modi_metadata) {
							calendar = exif.getDate(rootFolderPath + path);
						} else {
							calendar = datemod.getDate(rootFolderPath + path);
						}

						db.fillImages(i, path, calendar.get(Calendar.DAY_OF_MONTH),
								(calendar.get(Calendar.MONTH) + 1),
								calendar.get(Calendar.YEAR),
								calendar.get(Calendar.HOUR_OF_DAY),
								calendar.get(Calendar.MINUTE), size, 1234);

					} else {
						// nicht gefunden
						System.out.println("Nicht gefunden: " + i);
						missingTxt.println(i);
					}
				}
				missingTxt.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			double processingTime = ((System.currentTimeMillis() - timeStart)
					* 0.001);
			System.out.println("Process time: " + processingTime + " seconds.");
		} else {
			System.out.println("Troll");
			///////////////////////////////////////////////////////////////////
			DBController db = new DBController(projectPath);
			db.connectDatabase();
			QueryImages qi = new QueryImages(db, outputPath, rootFolderPath);

			// qi.outputImagesFromHour(0, 18, "copy", true);

			// qi.outputImagesFromHourToHour(0, 8, 19, "copy", false);

			qi.outputImagesEveryHour(0, 8, 18, "copy", false);

			// qi.outputImagesFromHourAndMinute(40177, 8, 30, "copy", true);

			// qi.outputImagesFromStartToEnd(16185, 16282, 10, "copy", false,
			// 2000000);
		}
	}
}
