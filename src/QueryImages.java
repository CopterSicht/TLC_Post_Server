import java.util.Calendar;
import java.util.List;

public class QueryImages
{

	private DBController db;

	private Output op;

	private String outputPath;

	private String rootFolderPath;

	public QueryImages(DBController pDB, String pOutputPath,
			String pRootFolderPath)
	{
		this.db = pDB;
		this.outputPath = pOutputPath;
		this.rootFolderPath = pRootFolderPath;
		op = new Output(outputPath);
		op.createFolder();
	}

	public boolean checkIfSunday(int day, int month, int year)
	{
		Calendar cal = Calendar.getInstance();

		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.MONTH, (month - 1));
		cal.set(Calendar.DAY_OF_MONTH, day);

		// System.out.println("CAL: " + cal.getTime());

		if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
			// System.out.println("Sunday! " + day + "." + month + "." + year);
			return false;
		}
		return true;
	}

	private boolean timeCheck(int pH1, int pM1, int pH2, int pM2, int pInterval)
	{
		int time1 = (pH1 * 60) + pM1;
		int time2 = (pH2 * 60) + pM2;

		if ((time2 - time1) >= pInterval) {
			return true;
		}
		return false;
	}

	public void outputImagesFromHour(int startcounter, int hour, String method,
			boolean sunday)
	{
		List<Image> resultList = db.query(
				"SELECT imageCounter, path, day, month, year, hour, minute, size, brightness  FROM images WHERE hour = "
						+ hour + " AND minute <= 5 AND  minute > 0 ;");

		for (int i = 0; i < resultList.size(); i++) {

			Image image = resultList.get(i);

			if (image.getImageCounter() < startcounter) {
				continue;
			}

			if (!sunday) {
				if (checkIfSunday(image.getDay(), image.getMonth(),
						image.getYear())) {
					System.out.println("Path = " + image.getPath());
					op.outputMethod(method, rootFolderPath, image.getPath());
				}
			} else {
				System.out.println("Path = " + image.getPath());
				op.outputMethod(method, rootFolderPath, image.getPath());
			}
		}
		resultList = null; // free
	}

	public void outputImagesFromHourToHour(int startcounter, int starthour,
			int endhour, String method, boolean sunday)
	{
		List<Image> resultList = db.query(
				"SELECT imageCounter, path, day, month, year, hour, minute, size, brightness  FROM images WHERE hour >= "
						+ starthour + " AND hour < " + endhour + ";");

		for (int i = 0; i < resultList.size(); i++) {

			Image image = resultList.get(i);

			if (image.getImageCounter() < startcounter) {
				continue;
			}

			if (!sunday) {
				if (checkIfSunday(image.getDay(), image.getMonth(),
						image.getYear())) {
					System.out.println("Path = " + image.getPath());
					op.outputMethod(method, rootFolderPath, image.getPath());
				}
			} else {
				System.out.println("Path = " + image.getPath());
				op.outputMethod(method, rootFolderPath, image.getPath());
			}
		}
		resultList = null; // free
	}

	public void outputImagesFromHourAndMinute(int startcounter, int hour,
			int minute, String method, boolean sunday)
	{
		// List<Image> resultList = db.query(
		// "SELECT imageCounter, path, day, month, year, hour, minute, size,
		// brightness FROM images WHERE hour = "
		// + hour + " AND minute >= ("+ (minute-3) +") AND minute <= ( "+
		// (minute+3) + ") ;");

		List<Image> resultList = db.query(
				"SELECT imageCounter, path, day, month, year, hour, minute, size, brightness  FROM images WHERE hour = "
						+ hour + " AND minute = " + minute + ";");

		for (int i = 0; i < resultList.size(); i++) {
			Image image = resultList.get(i);
			if (image.getImageCounter() < startcounter) {
				// System.out.println(image.getImageCounter());
				continue;
			}
			if (!sunday) {
				if (checkIfSunday(image.getDay(), image.getMonth(),
						image.getYear())) {
					System.out.println("Path = " + image.getPath());
					op.outputMethod(method, rootFolderPath, image.getPath());
				}
			} else {
				System.out.println("Path = " + image.getPath());
				op.outputMethod(method, rootFolderPath, image.getPath());
			}
		}
		resultList = null; // free
	}

	public void outputImagesFromStartToEnd(int pStart, int pEnd, int pMinute,
			String method, boolean sunday, int pSize)
	{
		op.createFolder();

		int hour = db.getHourFromIndex(pStart);
		int minute = db.getMinuteFromIndex(pStart);
		int day = db.getDayFromIndex(pStart);

		for (int i = pStart; i < pEnd; i++) {
			String path = db.getPathFromIndex(i);
			if ((path == "Error 404") || (path == null)) {
				continue;
			}

			if (day != db.getDayFromIndex(i)) {
				// neuer Tag
				day = db.getDayFromIndex(i);
				hour = db.getHourFromIndex(i);
				minute = db.getMinuteFromIndex(i);
			}

			if (timeCheck(hour, minute, db.getHourFromIndex(i),
					db.getMinuteFromIndex(i), pMinute)) {

				if (db.getSizeFromIndex(i) > pSize) {
					System.out.println("Pfad: " + db.getPathFromIndex(i));
					// System.out.println(db.getSizeFromIndex(i));
					hour = db.getHourFromIndex(i);
					minute = db.getMinuteFromIndex(i);
					op.outputMethod(method, rootFolderPath, path);
				}
			} else {
				// System.out.println("Nein: Pfad: " + db.getPathFromIndex(i));
			}
		}
	}

	public void outputImagesEveryHour(int startcounter, int starthour,
			int endhour, String method, boolean sunday)
	{
		List<Image> resultList = db.query(
				"SELECT imageCounter, path, day, month, year, hour, minute, size, brightness  FROM images WHERE (hour >= "
						+ starthour + " AND hour <= " + endhour
						+ ") AND minute <= 5 AND minute > 0 ;");

		for (int i = 0; i < resultList.size(); i++) {

			Image image = resultList.get(i);

			if (image.getImageCounter() < startcounter) {
				continue;
			}

			if (!sunday) {
				if (checkIfSunday(image.getDay(), image.getMonth(),
						image.getYear())) {
					System.out.println("Path = " + image.getPath());
					op.outputMethod(method, rootFolderPath, image.getPath());
				}
			} else {
				System.out.println("Path = " + image.getPath());
				op.outputMethod(method, rootFolderPath, image.getPath());
			}
		}
		resultList = null; // free
	}

}
