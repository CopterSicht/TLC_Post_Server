import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;

public class Output
{

	private String outputpath;

	private String foldername;

	public Output(String path)
	{
		outputpath = path;
	}

	public void createFolder()
	{

		Calendar calendar = Calendar.getInstance();
		foldername = Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)) + "."
				+ Integer.toString((calendar.get(Calendar.MONTH) + 1)) + "."
				+ Integer.toString(calendar.get(Calendar.YEAR)) + "_"
				+ Integer.toString(calendar.get(Calendar.HOUR_OF_DAY)) + ":"
				+ Integer.toString(calendar.get(Calendar.MINUTE)) + "/";

		// System.out.println("ordner: " + outputpath+foldername);
		File dir = new File(outputpath + foldername);
		dir.mkdir();

	}

	public void copyImage(String rootpath, String imagePath)
	{
		String imagename = imagePath.substring(imagePath.lastIndexOf("/") + 1);

		File src = new File(rootpath + imagePath);
		File dst = new File(outputpath + foldername + imagename);

		try {
			FileUtils.copyFile(src, dst);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Konnte Datei nicht kopieren!");
			e.printStackTrace();
		}
	}

	public void linkImage(String rootpath, String imagePath)
	{
		String imagename = imagePath.substring(imagePath.lastIndexOf("/") + 1);
		Path newLink = Paths.get(outputpath + foldername + imagename);
		Path target = Paths.get(rootpath + imagePath);
		try {
			Files.createSymbolicLink(newLink, target);
		} catch (IOException x) {
			System.err.println(x);
		} catch (UnsupportedOperationException x) {
			// Some file systems do not support symbolic links.
			System.err.println(x);
		}

	}

	public void outputMethod(String method, String rootpath, String imagePath)
	{
		if (method == "copy") {
			copyImage(rootpath, imagePath);
		}
		if (method == "link") {
			linkImage(rootpath, imagePath);
		}
	}
}
