
public class Image
{

	private int imageCounter;

	private String path;

	private int day;

	private int month;

	private int year;

	private int hour;

	private int minute;

	private int size;

	private int brightness;

	public Image(int pImageCounter, String pPath, int pDay, int pMonth,
			int pYear, int pHour, int pMinute, int pSize, int pBrightnes)
	{
		this.imageCounter = pImageCounter;
		this.path = pPath;
		this.day = pDay;
		this.month = pMonth;
		this.year = pYear;
		this.hour = pHour;
		this.minute = pMinute;
		this.size = pSize;
		this.brightness = pBrightnes;
	}

	public int getImageCounter()
	{
		return imageCounter;
	}

	public String getPath()
	{
		return path;
	}

	public int getDay()
	{
		return day;
	}

	public int getMonth()
	{
		return month;
	}

	public int getYear()
	{
		return year;
	}

	public int getHour()
	{
		return hour;
	}

	public int getMinute()
	{
		return minute;
	}

	public int getSize()
	{
		return size;
	}

	public int getBrightness()
	{
		return brightness;
	}

}
