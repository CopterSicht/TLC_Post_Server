
import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifSubIFDDirectory;
import java.io.File;
import java.io.IOException;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;

public class Exif
{
	public Calendar getDate(String path)
	{
		File file = new File(path);
		Date date = null;
		Calendar calendar = Calendar.getInstance();
		try {
			Metadata metadata = ImageMetadataReader.readMetadata(file);
			ExifSubIFDDirectory directory = metadata
					.getFirstDirectoryOfType(ExifSubIFDDirectory.class);
			date = directory.getDate(ExifSubIFDDirectory.TAG_DATETIME_ORIGINAL);
			ZonedDateTime cetTimeZoned = ZonedDateTime.of(
					new java.sql.Timestamp(date.getTime()).toLocalDateTime(),
					ZoneId.of("CET"));
			date = java.sql.Timestamp.valueOf(cetTimeZoned
					.withZoneSameInstant(ZoneOffset.UTC).toLocalDateTime());
			calendar.setTime(date);
			return calendar;
		} catch (ImageProcessingException e) {
		} catch (IOException e) {
		}
		return calendar;
	}

}
