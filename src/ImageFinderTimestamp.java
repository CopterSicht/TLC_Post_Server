import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;
import java.util.Comparator;

import org.apache.commons.io.filefilter.WildcardFileFilter;

public class ImageFinderTimestamp extends ImageFinder
{

	public ImageFinderTimestamp(String rootFolder)
	{
		super(rootFolder);
		// TODO Auto-generated constructor stub
	}

	static int filesindex;

	static int fileslenght;

	static File[] files = null;

	private static void sortImages()
	{

		for (int i = currentYear; i < years.length; i++) {
			if (years[i] != null) {

				for (int j = currentMonth; j < month[i].length; j++) {
					if (month[i][j]) {
						File dir = new File(
								rootFolder + years[i] + "/" + monthNames[j] + "/");

						files = dir.listFiles();
						fileslenght = files.length;

						// System.out.println("File lenght: " + fileslenght);

						sortFilesByDateCreated(files);

						filesindex = 0;

						currentMonth = j + 1;
						currentYear = i;
						return;
					}
				}
				currentMonth = 0;
			}
		}

	}

	public String getImagePath(int imageCounter)
	{
		String path = null;
		// System.out.println("filesindex: " + filesindex);
		// System.out.println("fileslenght: " + fileslenght);

		if ((filesindex < fileslenght) && (fileslenght != 0)) {
			path = files[filesindex].getAbsolutePath();
			filesindex++;
			return path.substring(lengthRootFolder);
		} else {
			// System.out.println("sort new Images");
			sortImages();

			if (filesindex >= fileslenght) {
				return "404";
			}

			path = files[filesindex].getAbsolutePath();
			filesindex++;
			return path.substring(lengthRootFolder);
		}

	}

	public static void sortFilesByDateCreated(File[] files)
	{
		Arrays.sort(files, new Comparator<File>() {
			public int compare(File f1, File f2)
			{
				long l1 = getFileCreationEpoch(f1);
				long l2 = getFileCreationEpoch(f2);
				return Long.valueOf(l1).compareTo(l2);
			}
		});
	}

	public static long getFileCreationEpoch(File file)
	{
		try {
			BasicFileAttributes attr = Files.readAttributes(file.toPath(),
					BasicFileAttributes.class);
			return attr.creationTime().toInstant().toEpochMilli();
		} catch (IOException e) {
			throw new RuntimeException(file.getAbsolutePath(), e);
		}
	}

}
