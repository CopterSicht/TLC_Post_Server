
import java.nio.file.Files;
import java.nio.file.Paths;

public abstract class ImageFinder
{

	static short maxYears = 5;

	int lengthRootFolder;

	static String rootFolder;

	static int currentYear = 0;

	static int currentMonth = 0;

	static String[] monthNames = { "01_January", "02_February", "03_March", "04_April",
			"05_May", "06_June", "07_July", "08_August", "09_September",
			"10_October", "11_November", "12_December" };

	static Boolean[][] month = new Boolean[maxYears][12];

	static String[] years = new String[maxYears];

	public ImageFinder(String rootFolder)
	{
		this.rootFolder = rootFolder;
		lengthRootFolder = rootFolder.length();
	}

	String findStartyear()
	{
		boolean next = true;
		int i = 0; // start year
		while (next) {
			int currentyear = i;
			if (Files.isDirectory(Paths.get(rootFolder + currentyear + "/"))) {
				next = false; // When a year folder found
			} else {
				i = i + 1;
				next = true;
			}
		}
		return Integer.toString(i); // return first year folder name
	}

	public void findYears(int yearCount, String startyear)
	{
		for (int i = 0; i <= years.length; i++) {
			int currentyear = Integer.parseInt(startyear) + (i * 1);
			if (Files.isDirectory(Paths.get(rootFolder + currentyear + "/"))) {
				// System.out.println("Jahr " + currentyear + " gefunden!");
				years[i] = Integer.toString(currentyear);
				yearCount = i + 1;
			} else {
				// System.out.println("Jahr " + currentyear + " nicht gefunden!");
			}
		}
	}

	public void findMonths(int yearCount)
	{
		for (int i = 0; i < years.length; i++) {
			// System.out.println(i + " Year-Array: " + years[i]);
			if (years[i] != null) {

				for (int j = 0; j < monthNames.length; j++) {
					if (Files.isDirectory(Paths
							.get(rootFolder + years[i] + "/" + monthNames[j] + "/"))) {
						month[i][j] = true;
					} else {
						month[i][j] = false;
					}
				}
			} else {
				break;
			}
		}
	}

	public void printFoundDirectories()
	{
		for (int i = 0; i < years.length; i++) {
			if (years[i] != null) {
				System.out.println("Jahr: " + years[i]);

				for (int j = 0; j < 12; j++) {
					if (month[i][j]) {
						System.out.print(monthNames[j] + ", ");
					}
				}
				System.out.println("");
			}
		}

	}

	protected String convertImageCounter(int imageCounter)
	{
		switch ((int) (Math.log10(imageCounter) + 1)) {
		case 1:
			return "00000" + Integer.toString(imageCounter);
		case 2:
			return "0000" + Integer.toString(imageCounter);
		case 3:
			return "000" + Integer.toString(imageCounter);
		case 4:
			return "00" + Integer.toString(imageCounter);
		case 5:
			return "0" + Integer.toString(imageCounter);
		case 6:
			return Integer.toString(imageCounter);
		default:
			return "000000";
		}
	}

	public abstract String getImagePath(int imageCounter);

}
